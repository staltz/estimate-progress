const estimateProgress = require('../index');

const KNOWN_TARGET = 1000;
const p = {start: 1, current: 1, target: 1};
setInterval(() => {
  p.current += 8 + Math.round(Math.random() * 5);
  if (p.current > p.target) p.target = Math.ceil(p.current * 1.1);
  if (p.target >= KNOWN_TARGET) p.target = KNOWN_TARGET;
  if (p.current >= KNOWN_TARGET) p.current = KNOWN_TARGET;
}, 80);
setInterval(() => {
  p.start = p.current * 0.9;
}, 700);

const getter = estimateProgress(() => p, 10, 0.9);
const realbar = document.getElementById('realbar');
const realtitle = document.getElementById('realtitle');
const estbar = document.getElementById('estbar');
const esttitle = document.getElementById('esttitle');
function render() {
  const x1 =
    p.current === p.target ? 1 : (p.current - p.start) / (p.target - p.start);
  realbar.style.width = `${x1 * 500}px`;
  realtitle.textContent = `${Math.round(x1 * 100)}%`;

  const p2 = getter();
  const x =
    p2.current === p2.target
      ? 1
      : (p2.current - p2.start) / (p2.target - p2.start);
  estbar.style.width = `${x * 500}px`;
  esttitle.textContent = `${Math.round(x * 100)}%`;
}
setInterval(render, 100);
