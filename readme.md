# estimate-progress

**Takes progress bar data that is too jumpy and makes the data "smoother"**.

## Installation

```sh
npm install estimate-progress
```

Note: uses syntactical ES5 but one API from ES6 (Array.fill).

## Problem

Suppose you have progress bar data that looks like this:

```
{
  start: number, // what number the progress started from
  current: number, // the current status of the progression
  target: number, // the goal number, once this is reached, supposedly we're done
}
```

And assume that `start <= current <= target`. If either the `start` or `target` of the progress data changes often, then the rendered progress bar will be "jumpy", going back to previous numbers.

## Solution

This library is a heuristic to estimate the "real final" `target` and keep the `start` fixed to the minimum observed. In particular, the calculation of the target estimation is simple unacademic math, using two techniques:

- Inertia: smoothens the rate of change of the moving target
- Margin: this is like a moving average but not actually an average, it just calculates the rate of change of the target N steps back, and uses that difference to extrapolate N steps into the future

The result is that the progress is much less jumpy, and as a side effect, the estimated progress data *always* completes some seconds after the actual completion (due to inertia).

![example progress bars](example.gif)

## Usage

```js
const estimateProgress = require('estimate-progress');

const getEstimatedProgress = estimateProgress(() => actualProgress);

console.log(getEstimatedProgress()); // {start: 2, current: 5, target: 9}
```

### API

`estimateProgress(getter1, /* both optional: */ margin, inertia): getter2`

- `getter1`: the first argument is a getter function that returns the actual progress data. It's necessary that this is a getter of data and not the actual data because we do calculations over time, and we assume the progress data evolves over time. This is the only required argument. Progress data should always be an object `{start, current, target}`
- `margin`: how many steps behind (and ahead) to use for calculations. By default, this is 10. Use big numbers (~50) if you think the progress will take minutes, use small numbers (~8) if you think the progress will take seconds. The bigger the margin number, the more memory is used.
- `inertia`: how conservatively should we move towards the moving target. This is a number between 0 and 1, by default 0.75. Use values close to 0 if you want the estimated progress to complete almost at the same time as the input progress. Use values close to 1 if you want the estimated progress to move smoothly (yet complete much after the input progress completes).

Returns a getter function that behaves like the input getter function.

## License

[MIT](LICENSE)