function isUndef(x) {
  return typeof x === 'undefined';
}

module.exports = function estimateProgress(getter, margin, inertia) {
  if (isUndef(getter)) throw new Error('Requires a getter as first argument');
  var N = isUndef(margin) ? 10 : margin;
  var q = isUndef(inertia) ? 0.75 : inertia;
  var initially = getter();
  var start = initially.start;
  var prevTargets = Array(N).fill(initially.target);
  var prevSlope = 0;
  return function() {
    var actual = getter();
    var realSlope = actual.target - prevTargets[0];
    var slope = q * prevSlope + (1 - q) * realSlope;
    var target = actual.target + slope * N;
    if (Math.abs(target - actual.target) / actual.target < 0.01) {
      target = actual.target;
    }
    for (var i = 0; i < N - 1; i++) {
      prevTargets[i] = prevTargets[i + 1];
    }
    prevTargets[N - 1] = actual.target;
    prevSlope = slope;
    return {start: start, current: actual.current, target: target};
  };
};
